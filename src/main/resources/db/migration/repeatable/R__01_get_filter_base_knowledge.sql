--get_filter_base_knowledge
DROP FUNCTION IF EXISTS get_filter_base_knowledge(p_title text, p_content text, p_note text, p_part text,
                                                  p_conclusion text, p_is_deleted boolean, p_is_active boolean,
                                                  p_id_user bigint, p_code_role text, p_before_time_create timestamp with time zone,
                                                  p_after_time_create timestamp with time zone);


CREATE OR REPLACE FUNCTION get_filter_base_knowledge(p_title text, p_content text, p_note text, p_part bigint,
                                                     p_conclusion text, p_is_deleted boolean, p_is_active boolean,
                                                     p_id_user bigint, p_code_role text,
                                                     p_id_author bigint[], p_id_type_knowledge bigint[],
                                                     p_before_time_create timestamp WITH TIME ZONE,
                                                     p_after_time_create timestamp WITH TIME ZONE)
    RETURNS SETOF knowledge_base
    LANGUAGE PLPGSQL
AS
$$
BEGIN
RETURN QUERY
    WITH privilege AS (
        SELECT cr.*
        FROM cls_role AS cr
            INNER JOIN reg_user_role AS rur
              ON cr.id = rur.id_role
        WHERE cr.id = p_id_user
          AND cr.code = p_code_role
        )
SELECT *
FROM knowledge_base AS kb
    INNER JOIN reg_user_knowledge AS ruk
      ON kb.id = ruk.id_knowledge
    INNER JOIN reg_type_knowledge_knowledge AS rtkk
      ON kb.id = rtkk.id_knowledge
WHERE (kb.title ILIKE '%' || p_title || '%' OR p_title IS NULL)
  AND (kb.content ILIKE '%' || p_content || '%' OR p_content IS NULL)
  AND (kb.note ILIKE '%' || p_note || '%' OR p_note IS NULl)
  AND (kb.conclusion ILIKE '%' || p_conclusion || '%' OR p_conclusion IS NULl)
  AND (kb.part = p_part OR p_part IS NULl)
  AND (kb.is_deleted = p_is_deleted)
  AND (kb.is_active = p_is_active)
  AND (kb.time_create >= p_before_time_create OR p_before_time_create IS NULL)
  AND (kb.time_create <= p_after_time_create OR p_after_time_create IS NULL)
  AND (kb.time_create <= p_after_time_create OR p_after_time_create IS NULL)
  AND (rtkk.id_type IN (p_id_type_knowledge) OR p_id_type_knowledge IS NULL)
  AND (kb.privilege IS NOT NULL
    OR p_code_role = (SELECT p.code FROM privilege AS p)
    OR ruk.id_author IN (p_id_author));
END;
$$;

