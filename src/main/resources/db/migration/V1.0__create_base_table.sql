--cls_role
CREATE TABLE IF NOT EXISTS cls_role
(
    id BIGSERIAL PRIMARY KEY,
    "name" VARCHAR(30) NOT NULL,
    code VARCHAR(30) NOT NULL,
    is_deleted BOOLEAN DEFAULT FALSE,
    time_create TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

--cls_user
CREATE TABLE IF NOT EXISTS cls_user
(
    id BIGSERIAL PRIMARY KEY,
    login VARCHAR(15) NOT NULL,
    password TEXT NOT NULL,
    firstname VARCHAR(30),
    lastname VARCHAR(30),
    is_deleted BOOLEAN DEFAULT FALSE,
    time_create TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

--reg_user_role
CREATE TABLE IF NOT EXISTS reg_user_role
(
    id BIGSERIAL PRIMARY KEY,
    id_user INTEGER
    CONSTRAINT fk_reg_user_role_cls_user
        REFERENCES cls_user,
    id_role INTEGER
    CONSTRAINT fk_reg_user_role_cls_role
        REFERENCES cls_role
);

--cls_type_knowledge
CREATE TABLE IF NOT EXISTS cls_type_knowledge
(
    id BIGSERIAL PRIMARY KEY,
    "name" VARCHAR(50),
    "level" INTEGER DEFAULT 1,
    code VARCHAR(50),
    is_deleted BOOLEAN DEFAULT FALSE,
    time_create TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

--cls_privilege_knowledge
CREATE TABLE IF NOT EXISTS cls_privilege_knowledge
(
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR(50),
    code VARCHAR(50)
);

--knowledge_base
CREATE TABLE IF NOT EXISTS knowledge_base
(
    id BIGSERIAL PRIMARY KEY,
    title VARCHAR(50) NOT NULL,
    content TEXT NOT NULL,
    note TEXT,
    part INTEGER,
    conclusion TEXT,
    is_deleted BOOLEAN DEFAULT FALSE,
    is_active BOOLEAN DEFAULT TRUE,
    id_knowledge INTEGER
    CONSTRAINT fk_knowledge_base_cls_privilege_knowledge
        REFERENCES cls_privilege_knowledge,
    time_create TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

--reg_user_knowledge
CREATE TABLE IF NOT EXISTS reg_user_knowledge
(
    id BIGSERIAL PRIMARY KEY,
    id_author INTEGER
    CONSTRAINT fk_reg_user_knowledge_cls_user
        REFERENCES cls_user,
    id_knowledge INTEGER
    CONSTRAINT fk_reg_user_knowledge_knowledge_base
        REFERENCES knowledge_base,
    is_deleted BOOLEAN DEFAULT FALSE
);

--reg_type_knowledge_knowledge
CREATE TABLE IF NOT EXISTS reg_type_knowledge_knowledge
(
    id BIGSERIAL PRIMARY KEY,
    id_type INTEGER
    CONSTRAINT fk_reg_type_knowledge_knowledge_cls_type_knowledge
        REFERENCES cls_type_knowledge,
    id_knowledge INTEGER
    CONSTRAINT fk_reg_type_knowledge_knowledge_knowledge_base
        REFERENCES knowledge_base
);

--cls_privilege
CREATE TABLE IF NOT EXISTS cls_privilege
(
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR(50),
    code VARCHAR(50)
);