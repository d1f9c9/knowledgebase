--cls_type_knowledge
INSERT INTO cls_type_knowledge (name, level, code) VALUES
('Программирование', 1, 'PROGRAM'),
('Языки программирования', 2, 'PROGRAM_LANGS'),
('Базы данных', 2, 'DATABASE'),
('PostgreSQL', 3, 'POSTGRES'),
('java', 3, 'JAVA'),
('java script', 3, 'JAVA_SCRIPT');

--cls_role
INSERT INTO cls_role (name, code) VALUES
('Гость', 'GUEST'),
('Пользователь', 'USER'),
('Администратор', 'ADMIN'),
('Администратор системы', 'ADMIN_SYSTEMS');

--cls_privilege_knowledge
INSERT INTO cls_privilege_knowledge (name, code) VALUES
('Недоступно', 'NO_ACCESS'),
('Чтение', 'READ'),
('Чтение, Редактирование', 'EDIT'),
('Чтение, Редактирование, Блокирование', 'BLOCK'),
('Чтение, Редактирование, Блокирование, Удаление', 'DELETE');

--ТЕСТОВЫЕ ДАННЫЕ
--cls_user
INSERT INTO cls_user (login, password, firstname, lastname) VALUES
('admin', 'pass', 'admin', 'admin');

--cls_user
INSERT INTO cls_user (login, password, firstname, lastname) VALUES
    ('admin', 'pass', 'admin', 'admin');

--knowledge_base
INSERT INTO knowledge_base (title, content, note, part, conclusion, id_knowledge) VALUES
('title', 'content', 'note', 1, 'conclusion', 1);

--reg_type_knowledge_knowledge
INSERT INTO reg_type_knowledge_knowledge (id_type, id_knowledge) VALUES
    (1, 1);

--reg_user_knowledge
INSERT INTO reg_user_knowledge (id_author, id_knowledge) VALUES
    (1, 1);