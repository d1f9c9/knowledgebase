package ru.adventurers.knowledgeBase.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.adventurers.knowledgeBase.model.KnowledgeBase;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface KnowledgeBaseRepo extends CrudRepository<KnowledgeBase, Long> {

    List<KnowledgeBase> findAllByIsDeleted(boolean is_deleted);
    Optional<KnowledgeBase> findByIdAndIsDeleted(Long idKnowledge, boolean is_deleted);


    @Query(nativeQuery = true, value = """
            SELECT *
            FROM get_filter_base_knowledge(:title, :content, :note, :part, :conclusion, 
                                           :id_author, :id_type_knowledge, :is_deleted,
                                           :is_active, :id_user, :code_role, :before_time,
                                           :after_time)
            """)
    List<KnowledgeBase> getFilter(@Param("id_author") List <Long> idAuthor, @Param("id_type_knowledge") List <Long> idTypeKnowledge,
                                  @Param("title") String title, @Param("content") String content, @Param("note") String  note,
                                  @Param("part") Long  part, @Param("conclusion") String conclusion, @Param("before_time") LocalDateTime beforeTime,
                                  @Param("after_time") LocalDateTime  afterTime,  @Param("is_deleted") boolean  isDeleted,  @Param("is_active") boolean  isActive,
                                  @Param("id_user") Long  idUser, @Param("code_role") String codeRole);
}
