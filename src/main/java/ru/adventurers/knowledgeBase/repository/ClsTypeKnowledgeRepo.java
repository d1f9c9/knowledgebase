package ru.adventurers.knowledgeBase.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.adventurers.knowledgeBase.model.ClsRole;
import ru.adventurers.knowledgeBase.model.ClsTypeKnowledge;

import java.util.List;

@Repository
public interface ClsTypeKnowledgeRepo extends CrudRepository<ClsTypeKnowledge, Long> {
    @Query(nativeQuery = true, value = """
            SELECT ctk.*
            FROM cls_type_knowledge AS ctk
                INNER JOIN reg_type_knowledge_knowledge AS rtkk
                  ON ctk.id = rtkk.id_type
                INNER JOIN knowledge_base AS kb
                  ON rtkk.id_knowledge = kb.id
            WHERE rtkk.id_knowledge = :id_knowledge
              AND kb.is_deleted = :is_deleted""")
    List<ClsTypeKnowledge> getTypeKnowledge(@Param("id_knowledge") Long idKnowledge,
                                            @Param("is_deleted") boolean isDeleted);

    List<ClsTypeKnowledge> findAllByIsDeleted(boolean isDeleted);
}
