package ru.adventurers.knowledgeBase.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.adventurers.knowledgeBase.model.ClsRole;

import java.util.List;

@Repository
public interface ClsRoleRepo extends CrudRepository<ClsRole, Long> {
    @Query(nativeQuery = true, value = """
            SELECT *
            FROM cls_role AS cr
                INNER JOIN reg_user_role AS rur
              ON cr.id = rur.id_role
            WHERE rur.id_user = :id_user""")
    List<ClsRole> getRolesUser(@Param("id_user") Long idRole);
}
