package ru.adventurers.knowledgeBase.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.adventurers.knowledgeBase.model.RegTypeKnowledgeKnowledge;
import ru.adventurers.knowledgeBase.model.RegUserRole;

import java.util.List;

@Repository
public interface RegUserRoleRepo extends CrudRepository<RegUserRole, Long> {
    @Query(nativeQuery = true, value = """
            SELECT *
            FROM reg_type_knowledge_knowledge AS rtkk
                INNER JOIN knowledge_base AS kb
                  ON kb.id = rtkk.id_knowledge
            WHERE rtkk.id_knowledge = :id_knowledge
              AND kb.is_deleted = :is_deleted""")
    List<RegTypeKnowledgeKnowledge> getRegTypeKnowledgeKnowledgesById(@Param("id_knowledge") Long idKnowledge,
                                                                      @Param("is_deleted")boolean isDeleted);
}
