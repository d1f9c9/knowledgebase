package ru.adventurers.knowledgeBase.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.adventurers.knowledgeBase.model.KnowledgeBase;
import ru.adventurers.knowledgeBase.model.RegUserKnowledge;

import java.util.List;

@Repository
public interface RegUserKnowledgeRepo extends CrudRepository<RegUserKnowledge, Long> {
    List<RegUserKnowledge> findAllByKnowledge(KnowledgeBase knowledgeBase);
}
