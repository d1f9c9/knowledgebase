package ru.adventurers.knowledgeBase.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "reg_user_role", schema = "public")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder(toBuilder = true)
public class RegUserRole {
    @Id
    @Column(name = "id", nullable = false)
    @SequenceGenerator(name = "REG_USER_ROLE_SEQ_GEN", sequenceName = "reg_user_role_id_seq", allocationSize = 1, schema = "public")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REG_USER_ROLE_SEQ_GEN")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "id_author", referencedColumnName = "id")
    private ClsUser author;
    @ManyToOne
    @JoinColumn(name = "id_role", referencedColumnName = "id")
    private ClsRole role;

    @Override
    public boolean equals(Object object) {
        boolean result = this == object;

        if (!result && object != null && getClass() == object.getClass()) {
            RegUserRole that = (RegUserRole) object;

            result = Objects.equals(id, that.id)
                    && Objects.equals(author, that.author)
                    && Objects.equals(role, that.role);
        }

        return result;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, author, role);
    }
}
