package ru.adventurers.knowledgeBase.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "cls_role", schema = "public")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder(toBuilder = true)
public class ClsRole extends BaseTimeCreate{
    @Id
    @Column(name = "id", nullable = false)
    @SequenceGenerator(name = "CLS_ROLE_SEQ_GEN", sequenceName = "cls_role_id_seq", allocationSize = 1, schema = "public")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CLS_ROLE_SEQ_GEN")
    private Long id;

    @Basic
    @Column(name = "name", nullable = false)
    private String name;
    @Basic
    @Column(name = "code", nullable = false)
    private String code;
    @Basic
    @Column(name = "time_create", nullable = false)
    private LocalDateTime timeCreate;


    @Override
    public boolean equals(Object object) {
        boolean result = this == object;

        if (!result && object != null && getClass() == object.getClass()) {
            ClsRole that = (ClsRole) object;

            result = Objects.equals(id, that.id)
                    && Objects.equals(name, that.name)
                    && Objects.equals(code, that.code)
                    && Objects.equals(timeCreate, that.timeCreate);
        }

        return result;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, code, name, timeCreate);
    }

}
