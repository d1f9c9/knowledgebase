package ru.adventurers.knowledgeBase.model;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "cls_user", schema = "public")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder(toBuilder = true)
public class ClsUser extends BaseTimeCreate {
    @Id
    @Column(name = "id", nullable = false)
    @SequenceGenerator(name = "CLS_USER_SEQ_GEN", sequenceName = "cls_user_id_seq", allocationSize = 1, schema = "public")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CLS_USER_SEQ_GEN")
    private Long id;
    @Basic
    @Column(name = "login", nullable = false)
    private String login;
    @Basic
    @Column(name = "password", nullable = false)
    private String password;
    @Basic
    @Column(name = "firstname", nullable = false)
    private String firstname;
    @Basic
    @Column(name = "lastname", nullable = false)
    private String lastname;

    @Override
    public boolean equals(Object object) {
        boolean result = this == object;

        if (!result && object != null && getClass() == object.getClass()) {
            ClsUser that = (ClsUser) object;

            result = Objects.equals(id, that.id)
                    && Objects.equals(login, that.login)
                    && Objects.equals(firstname, that.firstname)
                    && Objects.equals(lastname, that.lastname)
                    && Objects.equals(timeCreate, that.timeCreate);
        }

        return result;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, login, firstname, lastname, timeCreate);
    }
}
