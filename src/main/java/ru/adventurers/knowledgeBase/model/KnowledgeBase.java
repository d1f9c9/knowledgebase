package ru.adventurers.knowledgeBase.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import ru.adventurers.knowledgeBase.dto.KnowledgeBaseDto;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "knowledge_base", schema = "public")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder(toBuilder = true)
public class KnowledgeBase extends BaseTimeCreate{
    @Id
    @Column(name = "id", nullable = false)
    @SequenceGenerator(name = "KNOWLEDGE_BASE_SEQ_GEN", sequenceName = "knowledge_base_id_seq", allocationSize = 1, schema = "public")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "KNOWLEDGE_BASE_SEQ_GEN")
    private Long id;
    @Basic
    @Column(name = "title", nullable = false)
    private String title;
    @Basic
    @Column(name = "content", nullable = false)
    private String content;

    @Basic
    @Column(name = "note", nullable = false)
    private String note;

    @Basic
    @Column(name = "part", nullable = false)
    private Long part;
    @Basic
    @Column(name = "conclusion", nullable = false)
    private String conclusion;

    @Basic
    @Column(name = "is_deleted", nullable = false)
    private boolean isDeleted;

    @Basic
    @Column(name = "is_active", nullable = false)
    private boolean isActive;

    public KnowledgeBase(KnowledgeBaseDto knowledgeBaseDto) {
        this.id = knowledgeBaseDto.getId();
        this.title = knowledgeBaseDto.getTitle();
        this.conclusion = knowledgeBaseDto.getConclusion();
        this.note = knowledgeBaseDto.getNote();
        this.part = knowledgeBaseDto.getPart();
        this.conclusion = knowledgeBaseDto.getConclusion();
        this.timeCreate = knowledgeBaseDto.getTimeCreate();
    }

    @Override
    public boolean equals(Object object) {
        boolean result = this == object;

        if (!result && object != null && getClass() == object.getClass()) {
            KnowledgeBase that = (KnowledgeBase) object;

            result = Objects.equals(id, that.id)
                    && Objects.equals(title, that.title)
                    && Objects.equals(content, that.content)
                    && Objects.equals(note, that.note)
                    && Objects.equals(part, that.part)
                    && Objects.equals(conclusion, that.conclusion)
                    && Objects.equals(timeCreate, that.timeCreate);
        }

        return result;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, content, note, part, conclusion, timeCreate);
    }
}
