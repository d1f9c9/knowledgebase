package ru.adventurers.knowledgeBase.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "reg_type_knowledge_knowledge", schema = "public")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder(toBuilder = true)
public class RegTypeKnowledgeKnowledge {
    @Id
    @Column(name = "id", nullable = false)
    @SequenceGenerator(name = "REG_TYPE_KNOWLEDGE_KNOWLEDGE_SEQ_GEN", sequenceName = "reg_type_knowledge_knowledge_id_seq", allocationSize = 1, schema = "public")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REG_TYPE_KNOWLEDGE_KNOWLEDGE_SEQ_GEN")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "id_type", referencedColumnName = "id")
    private ClsTypeKnowledge type;
    @ManyToOne
    @JoinColumn(name = "id_knowledge", referencedColumnName = "id")
    private KnowledgeBase knowledge;

    public RegTypeKnowledgeKnowledge(ClsTypeKnowledge type, KnowledgeBase knowledge) {
        this.type = type;
        this.knowledge = knowledge;
    }

    @Override
    public boolean equals(Object object) {
        boolean result = this == object;

        if (!result && object != null && getClass() == object.getClass()) {
            RegTypeKnowledgeKnowledge that = (RegTypeKnowledgeKnowledge) object;

            result = Objects.equals(id, that.id)
                    && Objects.equals(type, that.type)
                    && Objects.equals(knowledge, that.knowledge);
        }

        return result;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, knowledge);
    }
}
