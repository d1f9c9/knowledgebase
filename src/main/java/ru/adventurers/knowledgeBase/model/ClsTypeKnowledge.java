package ru.adventurers.knowledgeBase.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import ru.adventurers.knowledgeBase.dto.ClsTypeKnowledgeDto;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "cls_type_knowledge", schema = "public")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder(toBuilder = true)
public class ClsTypeKnowledge extends BaseTimeCreate {
    @Id
    @Column(name = "id", nullable = false)
    @SequenceGenerator(name = "CLS_TYPE_KNOWLEDGE_SEQ_GEN", sequenceName = "cls_type_knowledge_id_seq", allocationSize = 1, schema = "public")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CLS_TYPE_KNOWLEDGE_SEQ_GEN")
    private Long id;

    @Basic
    @Column(name = "name", nullable = false)
    private String name;

    @Basic
    @Column(name = "level", nullable = false)
    private Long level;
    @Basic
    @Column(name = "code", nullable = false)
    private String code;
    @Basic
    @Column(name = "is_deleted", nullable = false)
    private boolean isDeleted;

    public ClsTypeKnowledge(ClsTypeKnowledgeDto knowledgeTypeDto) {
        this.id = knowledgeTypeDto.getId();
        this.name = knowledgeTypeDto.getValue();
        this.level = knowledgeTypeDto.getLevel();
        this.code = knowledgeTypeDto.getCode();
    }

    @Override
    public boolean equals(Object object) {
        boolean result = this == object;

        if (!result && object != null && getClass() == object.getClass()) {
            ClsTypeKnowledge that = (ClsTypeKnowledge) object;

            result = Objects.equals(id, that.id)
                    && Objects.equals(name, that.name)
                    && Objects.equals(level, that.level)
                    && Objects.equals(code, that.code)
                    && Objects.equals(timeCreate, that.timeCreate);
        }

        return result;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, level, code, timeCreate);
    }
}
