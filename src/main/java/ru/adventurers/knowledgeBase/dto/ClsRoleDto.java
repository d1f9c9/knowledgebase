package ru.adventurers.knowledgeBase.dto;

import ru.adventurers.knowledgeBase.model.ClsRole;

public class ClsRoleDto {
    private Long id;
    private String name;
    private String code;

    public ClsRoleDto(ClsRole role) {
        this.id = role.getId();
        this.name = role.getName();
        this.code = role.getCode();
    }
}
