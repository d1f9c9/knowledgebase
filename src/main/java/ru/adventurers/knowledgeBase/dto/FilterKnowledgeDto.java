package ru.adventurers.knowledgeBase.dto;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class FilterKnowledgeDto {
    private Long id;
    private List<ClsUserDto> authors;
    private String title;
    private String content;
    private String note;
    private Long part;
    private List<ClsTypeKnowledgeDto> typeKnowledges;
    private String conclusion;
    private LocalDateTime beforeTimeCreate;
    private LocalDateTime afterTimeCreate;
    private boolean isDeleted;
    private boolean isActive;
    private Long idUser;
}
