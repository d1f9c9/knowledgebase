package ru.adventurers.knowledgeBase.dto;

import lombok.Data;
import ru.adventurers.knowledgeBase.model.ClsTypeKnowledge;

@Data
public class ClsTypeKnowledgeDto {
    private Long id;
    private String value;
    private Long level;
    private String code;

    public ClsTypeKnowledgeDto(ClsTypeKnowledge typeKnowledge) {
        this.id = typeKnowledge.getId();
        this.value = typeKnowledge.getName();
        this.level = typeKnowledge.getLevel();
        this.code = typeKnowledge.getCode();
    }
}
