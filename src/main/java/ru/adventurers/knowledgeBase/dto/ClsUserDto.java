package ru.adventurers.knowledgeBase.dto;

import lombok.Data;
import ru.adventurers.knowledgeBase.model.ClsUser;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class ClsUserDto {
    private Long id;
    private String login;
    private String firstname;
    private String lastname;
    private List<ClsRoleDto> roles;
    private LocalDateTime createTime;

    public ClsUserDto(ClsUser user, List<ClsRoleDto> roles) {
        this.id = user.getId();
        this.login = user.getLogin();
        this.firstname = user.getFirstname();
        this.lastname = user.getLastname();
        this.roles = roles;
        this.createTime = user.getTimeCreate();
    }
}
