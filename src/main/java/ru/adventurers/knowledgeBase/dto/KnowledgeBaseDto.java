package ru.adventurers.knowledgeBase.dto;

import lombok.Data;
import ru.adventurers.knowledgeBase.model.KnowledgeBase;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class KnowledgeBaseDto {
    private Long id;
    private List<ClsUserDto> authors;
    private String title;
    private String content;
    private String note;
    private Long part;
    private List<ClsTypeKnowledgeDto> typeKnowledges;
    private String conclusion;
    private LocalDateTime timeCreate;
    private boolean isActive;

    public KnowledgeBaseDto(KnowledgeBase knowledge, List<ClsUserDto> users, List<ClsTypeKnowledgeDto> types) {
        this.id = knowledge.getId();
        this.authors = users;
        this.title = knowledge.getTitle();
        this.content = knowledge.getContent();
        this.note = knowledge.getNote();
        this.part = knowledge.getPart();
        this.typeKnowledges = types;
        this.conclusion = knowledge.getConclusion();
        this.timeCreate = knowledge.getTimeCreate();
        this.isActive = knowledge.isActive();

    }

    public KnowledgeBaseDto(KnowledgeBase knowledge) {
        this.id = knowledge.getId();
        this.title = knowledge.getTitle();
        this.content = knowledge.getContent();
        this.note = knowledge.getNote();
        this.part = knowledge.getPart();
        this.conclusion = knowledge.getConclusion();
        this.timeCreate = knowledge.getTimeCreate();
        this.isActive = knowledge.isActive();
    }
}
