package ru.adventurers.knowledgeBase.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.adventurers.knowledgeBase.dto.ClsTypeKnowledgeDto;
import ru.adventurers.knowledgeBase.service.TypeKnowledgeService;

import java.util.List;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("type/knowledge")
public class TypeKnowledgeController {
    @Autowired
    TypeKnowledgeService typeKnowledgeService;

    @GetMapping("all")
    List<ClsTypeKnowledgeDto> getAllTypeKnowledge() {
        List<ClsTypeKnowledgeDto> types = typeKnowledgeService.getAllTypeKnowledge();

        return types;
    }
}
