package ru.adventurers.knowledgeBase.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.adventurers.knowledgeBase.dto.FilterKnowledgeDto;
import ru.adventurers.knowledgeBase.dto.KnowledgeBaseDto;
import ru.adventurers.knowledgeBase.service.KnowledgeService;

import java.util.List;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("knowledge")

public class KnowledgeController {
    @Autowired
    KnowledgeService knowledgeService;

    @PostMapping
    KnowledgeBaseDto addKnowledge(@RequestBody KnowledgeBaseDto knowledgeBaseDto) {
        KnowledgeBaseDto newKnowledgeBaseDto = knowledgeService.addKnowledge(knowledgeBaseDto);

        return newKnowledgeBaseDto;
    }

    @GetMapping("/{id_knowledge}/")
    KnowledgeBaseDto getKnowledgeBase(@PathVariable("id_knowledge") Long idKnowledge) {
        KnowledgeBaseDto newKnowledgeBaseDto = knowledgeService.getKnowledge(idKnowledge);

        return newKnowledgeBaseDto;
    }

    @DeleteMapping("/{id_knowledge}/")
    String deleteKnowledgeBase(@PathVariable("id_knowledge") Long idKnowledge) {
        knowledgeService.deleteKnowledge(idKnowledge);

        return "Успешно удалено!";
    }

    @PutMapping("/{id_knowledge}/")
    KnowledgeBaseDto updateKnowledgeBase(@RequestBody KnowledgeBaseDto knowledgeBaseDto) {
        KnowledgeBaseDto knowledgeBase = knowledgeService.updateKnowledge(knowledgeBaseDto);

        return knowledgeBase;
    }

    @GetMapping("all")
    List<KnowledgeBaseDto> getAllKnowledgeBase() {
        List<KnowledgeBaseDto> knowledgeBaseDto = knowledgeService.getAllKnowledge();

        return knowledgeBaseDto;
    }

    @PostMapping("filter")
    List<KnowledgeBaseDto> getFilterKnowledgeBase(@RequestBody FilterKnowledgeDto filter) {
        List<KnowledgeBaseDto> filteredKnowledge = knowledgeService.getFilter(filter);

        return filteredKnowledge;
    }
}
