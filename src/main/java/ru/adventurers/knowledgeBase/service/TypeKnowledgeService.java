package ru.adventurers.knowledgeBase.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.adventurers.knowledgeBase.dto.ClsTypeKnowledgeDto;
import ru.adventurers.knowledgeBase.model.ClsTypeKnowledge;
import ru.adventurers.knowledgeBase.repository.ClsTypeKnowledgeRepo;

import java.util.List;

@Service
public class TypeKnowledgeService {
    @Autowired
    ClsTypeKnowledgeRepo clsTypeKnowledgeRepo;

    public List<ClsTypeKnowledgeDto> getAllTypeKnowledge() {
        List<ClsTypeKnowledge> types = clsTypeKnowledgeRepo.findAllByIsDeleted(false);
        List<ClsTypeKnowledgeDto> typesDto = types.stream().map(ClsTypeKnowledgeDto::new).toList();

        return typesDto;
    }
}
