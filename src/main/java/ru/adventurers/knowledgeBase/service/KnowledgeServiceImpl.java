package ru.adventurers.knowledgeBase.service;

import ru.adventurers.knowledgeBase.dto.FilterKnowledgeDto;
import ru.adventurers.knowledgeBase.dto.KnowledgeBaseDto;

import java.util.List;

public interface KnowledgeServiceImpl {
    KnowledgeBaseDto addKnowledge(KnowledgeBaseDto knowledge);

    KnowledgeBaseDto getKnowledge(Long idKnowledge);

    List<KnowledgeBaseDto> getAllKnowledge();

    void deleteKnowledge(Long idKnowledge);

    KnowledgeBaseDto updateKnowledge(KnowledgeBaseDto knowledgeBaseDto);

    List<KnowledgeBaseDto> getFilter(FilterKnowledgeDto filter);
}
