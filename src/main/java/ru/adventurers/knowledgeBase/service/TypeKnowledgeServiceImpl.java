package ru.adventurers.knowledgeBase.service;

import ru.adventurers.knowledgeBase.dto.ClsTypeKnowledgeDto;

import java.util.List;

public interface TypeKnowledgeServiceImpl {
    List<ClsTypeKnowledgeDto> getAllTypeKnowledge();
}
