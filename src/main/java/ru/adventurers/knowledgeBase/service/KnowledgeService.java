package ru.adventurers.knowledgeBase.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.adventurers.knowledgeBase.dto.*;
import ru.adventurers.knowledgeBase.model.*;
import ru.adventurers.knowledgeBase.repository.*;
import ru.adventurers.knowledgeBase.utils.DefaultValue;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class KnowledgeService implements KnowledgeServiceImpl {

    @Autowired
    KnowledgeBaseRepo knowledgeBaseRepo;

    @Autowired
    RegTypeKnowledgeKnowledgeRepo regTypeKnowledgeKnowledgeRepo;

    @Autowired
    RegUserKnowledgeRepo regUserKnowledgeRepo;

    @Autowired
    ClsRoleRepo clsRoleRepo;

    @Autowired
    ClsTypeKnowledgeRepo clsTypeKnowledgeRepo;

    private KnowledgeBaseDto getOneKnowledge (Long idKnowledge) {
        KnowledgeBaseDto knowledgeBaseDto = null;
        KnowledgeBase knowledgeBase = knowledgeBaseRepo.findByIdAndIsDeleted(idKnowledge, false).orElse(null);

        if (knowledgeBase != null) {
            List<RegUserKnowledge> regUserKnowledge = regUserKnowledgeRepo.findAllByKnowledge(knowledgeBase);

            List<ClsUser> users = regUserKnowledge.stream().map(RegUserKnowledge::getAuthor).toList();

            List<ClsUserDto> usersDto = users.stream().map(user -> {
                List<ClsRole> roles = clsRoleRepo.getRolesUser(user.getId());
                List<ClsRoleDto> rolesDto = roles.stream().map(ClsRoleDto::new).toList();
                ClsUserDto usersDtos = new ClsUserDto(user, rolesDto);

                return usersDtos;
            }).toList();

            List<ClsTypeKnowledge> types = clsTypeKnowledgeRepo.getTypeKnowledge(knowledgeBase.getId(), false);
            List<ClsTypeKnowledgeDto> typesDto = types.stream().map(ClsTypeKnowledgeDto::new).toList();
            knowledgeBaseDto = new KnowledgeBaseDto(knowledgeBase, usersDto, typesDto);
        }

        return knowledgeBaseDto;
    }

    @Override
    public KnowledgeBaseDto addKnowledge(KnowledgeBaseDto knowledgeBaseDto) {
        KnowledgeBase knowledgeBase = new KnowledgeBase(knowledgeBaseDto);
        List<RegTypeKnowledgeKnowledge> regTypeKnowledgeKnowledges = knowledgeBaseDto.getTypeKnowledges().stream()
                .map(type -> new RegTypeKnowledgeKnowledge(new ClsTypeKnowledge(type), knowledgeBase)).toList();

        knowledgeBaseRepo.save(knowledgeBase);
        regTypeKnowledgeKnowledgeRepo.saveAll(regTypeKnowledgeKnowledges);

        return knowledgeBaseDto;
    }

    @Override
    public KnowledgeBaseDto getKnowledge(Long idKnowledge) {
        KnowledgeBaseDto knowledgeBaseDto = getOneKnowledge(idKnowledge);

        return knowledgeBaseDto;
    }

    @Override
    public List<KnowledgeBaseDto> getAllKnowledge() {
        List<KnowledgeBase> knowledgeBases = knowledgeBaseRepo.findAllByIsDeleted(false);
        List<KnowledgeBas           eDto> knowledgeBaseDtos = knowledgeBases.stream()
                .map(knowledge -> getOneKnowledge(knowledge.getId())).toList();

        return knowledgeBaseDtos;
    }

    @Override
    public void deleteKnowledge(Long idKnowledge) {
        KnowledgeBaseDto knowledgeBaseDto = getOneKnowledge(idKnowledge);
        KnowledgeBase knowledgeBase = new KnowledgeBase(knowledgeBaseDto);
        knowledgeBase.setDeleted(true);
    }

    @Override
    public KnowledgeBaseDto updateKnowledge(KnowledgeBaseDto knowledgeBaseDto) {
        List<ClsTypeKnowledge> types = clsTypeKnowledgeRepo.getTypeKnowledge(knowledgeBaseDto.getId(), false);

        List<ClsTypeKnowledgeDto> typesDto = knowledgeBaseDto.getTypeKnowledges();
        List<Long> typesIds = typesDto.stream().map(ClsTypeKnowledgeDto::getId).toList();

        List<ClsTypeKnowledge> typesDelete = types.stream()
                .filter(type -> !typesIds.contains(type.getId())).toList();

        List<ClsTypeKnowledge> typesAdd = typesDto.stream()
                .filter(typeDto -> !typesIds.contains(typeDto.getId()))
                .map(ClsTypeKnowledge::new).toList();

        clsTypeKnowledgeRepo.deleteAll(typesDelete);
        clsTypeKnowledgeRepo.saveAll(typesAdd);


        return knowledgeBaseDto;
    }

    @Override
    public List<KnowledgeBaseDto> getFilter(FilterKnowledgeDto filter) {
        final List<Long> idAuthor = filter.getAuthors() != null ? filter.getAuthors().stream().map(ClsUserDto::getId).toList() : null;
        final List<Long> idTypeKnowledge = filter.getTypeKnowledges() != null ? filter.getTypeKnowledges().stream().map(ClsTypeKnowledgeDto::getId).toList() : null;
        final String title = filter.getTitle();
        final String content = filter.getContent();
        final String note = filter.getNote();
        final Long part = filter.getPart();
        final String conclusion = filter.getConclusion();
        final LocalDateTime before = filter.getBeforeTimeCreate();
        final LocalDateTime after = filter.getAfterTimeCreate();
        final boolean isDeleted = filter.isDeleted();
        final boolean isActive = filter.isActive();
        final Long idUser = filter.getIdUser();

        List<KnowledgeBase> filteredKnowledge = knowledgeBaseRepo.getFilter(idAuthor, idTypeKnowledge, title,
                content, note, part, conclusion, before, after, isDeleted, isActive, idUser, DefaultValue.CODE_ROLE_ADMIN_SYSTEM);
        List<KnowledgeBaseDto> filteredKnowledgeDto = filteredKnowledge.stream().map(filt -> getOneKnowledge(filt.getId())).toList();

        return filteredKnowledgeDto;
    }



}
